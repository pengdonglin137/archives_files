# bpftrace
- 编译好的可执行程序

1. ./bpftrace_bin/x86_64/bpftrace: 静态编译版本，v0.12.0

```bash
./bpftrace ./trace.bt
./bpftrace -e 'BEGIN {printf("Hello World\n");}'
```

2. ./bpftrace_bin/aarch64/bpftrace: 静态编译版本，v0.12.0

```bash
./bpftrace ./trace.bt
./bpftrace -e 'BEGIN {printf("Hello World\n");}'
```

3. ./bpftrace_bin/x86_64/bpftrace_docker: 基于容器的版本：v0.16.0

```bash
./bpftrace.sh ./trace.bt
./bpftrace.sh -e 'BEGIN {printf("Hello World\n");}'
```

# Bcc

1. ./bcc_bin/x86_64/bcc_docker/bcc.sh

```bash
./bcc.sh vfscount            # use builtin bcc python scripts
./bcc.sh vfscount.bt         # use builtin bpftrace scripts
./bcc.sh ./<python script>   # use customized  bcc or bpftrace scripts
```

2. ./bcc_bin/x86_64/bcc_docker/bpftrace.sh

```bash
./bpftrace.sh ./trace.bt
./bpftrace.sh -e 'BEGIN {printf("Hello World\n");}'
```

3. ./bcc_bin/arm64/bcc_docker/bcc.sh

```bash
./bcc.sh vfscount            # use builtin bcc python scripts
./bcc.sh vfscount.bt         # use builtin bpftrace scripts
./bcc.sh ./<python script>   # use customized  bcc or bpftrace scripts
```

4. ./bcc_bin/arm64/bcc_docker/bpftrace.sh

```bash
./bpftrace.sh ./trace.bt
./bpftrace.sh -e 'BEGIN {printf("Hello World\n");}'
```

# crash

1. ./crash/x86_64/crash: 用于分析内核宕机的crash工具，版本为8.0.1

