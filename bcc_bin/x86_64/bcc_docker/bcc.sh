#!/bin/bash

mountpoint -q /sys/kernel/debug || mount -t debugfs debugfs /sys/kernel/debug/
if [ $? -ne 0 ];then
	echo "debugfs not mount"
	exit 1
fi

docker=docker
#docker="nerdctl -n k8s.io"

repository=ebpf
version=v1.6
image_name=${repository}_$(echo $version | sed 's/\./_/g').tar.xz

is_exist=$($docker image ls | grep ${repository} | grep ${version})
if [ -z "$is_exist" ]; then
	image_path=$(realpath $0)
	image_path=$(dirname $image_path)
	if [ ! -e ${image_path}/$image_name ];then
		echo "not found: ${image_path}/$image_name"
		exit 1
	fi
	echo "begin to load $image_name"
	xz -cd ${image_path}/$image_name | $docker load
fi

$docker run --network host --pid host -it --privileged --rm -w $(pwd) \
	-e PATH=/usr/local/bin/bpftrace_tools:/usr/local/bin/bcc_tools:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
	-v /lib/modules:/lib/modules:ro \
	-v /sys:/sys -v /tmp:/tmp \
	-v $(pwd):$(pwd) -v /sys/kernel/debug:/sys/kernel/debug \
	$repository:$version "$@"

