#!/bin/bash

mountpoint -q /sys/kernel/debug || mount -t debugfs debugfs /sys/kernel/debug/
if [ $? -ne 0 ];then
	echo "debugfs not mount"
	exit 1
fi

docker=docker
#docker="nerdctl -n k8s.io"

repository=bpftrace
version=v0.16
image_name=${repository}_$(echo $version | sed 's/\./_/g').tar

is_exist=$($docker image ls | grep ${repository} | grep ${version})
if [ -z "$is_exist" ]; then
	image_path=$(realpath $0)
	image_path=$(dirname $image_path)
	$docker load --input ${image_path}/$image_name
fi

$docker run -it --net=host --pid=host --privileged --rm -w $(pwd) \
	-v $(pwd):$(pwd):rw -v /sys/kernel/debug:/sys/kernel/debug:rw \
	-v /usr/src:/usr/src:ro -v /lib/modules/:/lib/modules:ro\
	$repository:$version "bpftrace" "$@"
